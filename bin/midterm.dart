import 'dart:io';

void main(List<String> arguments) {
  String? input = stdin.readLineSync();
  List<String>? list = toList(input!);
  List<String> postfix = toPostfix(list);

  print(EvaluatePostfix(postfix));
}

List<String> toPostfix(List<String> list) {
  List<String> operators = [];
  List<String> postfix = [];
  list.forEach((token) {
    if (int.tryParse(token) != null) {
      postfix.add(token);
    }
    if (isOperator(token)) {
      while (!operators.isEmpty &&
          operators.last != "(" &&
          precedence(token) <= precedence(operators.last)) {
        postfix.add(operators.removeLast());
      }
      operators.add(token);
    }
    if (token == "(") {
      operators.add(token);
    }
    if (token == ")") {
      while (operators.last != "(") {
        postfix.add(operators.removeLast());
      }
      operators.removeLast();
    }
  });

  while (!operators.isEmpty) {
    postfix.add(operators.removeLast());
  }

  return postfix;
}

List<String> toList(String input) {
  List<String>? list = input.split(" ");
  return list;
}

bool isOperator(String token) {
  switch (token) {
    case "+":
    case "-":
    case "*":
    case "/":
      return true;
  }
  return false;
}

int precedence(String token) {
  switch (token) {
    case "+":
    case "-":
      return 1;
    case "*":
    case "/":
      return 2;
    case "(":
      return 3;
  }
  return -1;
}

double EvaluatePostfix(List<String> postfix) {
  List<double> values = [];
  postfix.forEach((token) {
    if (int.tryParse(token) != null) {
      values.add(double.parse(token));
    } else {
      double right = values.removeLast();
      double left = values.removeLast();
      switch (token) {
        case "+":
          values.add(left + right);
          break;
        case "-":
          values.add(left - right);
          break;
        case "*":
          values.add(left * right);
          break;
        case "/":
          values.add(left / right);
          break;
        default:
          break;
      }
    }
  });

  return values.first;
}
